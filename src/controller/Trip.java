package controller;

public class Trip {

	private String id ;
	private String start_time;
	private String end_time;
	private String bikeId;
	private String duration;
	private String fromStationId;
	private String fStationName;
	private String tStationId;
	private String tStationName;
	private String type;
	private String gender;
	private String bYear;

	public Trip(String id, String start_time, String end_time, String bikeId,
			String duration, String fStationId, String fStationName,
			String tStationId, String tStationName, String type, String gender,
			String bYear){
		
		this.setId(id);
		this.setStart_time(start_time);
		this.setEnd_time(end_time);
		this.setBikeId(bikeId);
		this.setDuration(duration);
		this.setfStationId(fStationId);
		this.setfStationName(fStationName);
		this.settStationId(tStationId);
		this.settStationName(tStationName);
		this.setType(type);
		this.setGender(gender);
		this.setbYear(bYear);
	}

	public String id() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getBikeId() {
		return bikeId;
	}

	public void setBikeId(String bikeId) {
		this.bikeId = bikeId;
	}

	public String getDuration() {
		return duration;
	}
	

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFromStationId() {
		return fromStationId;
	}

	public void setfStationId(String fStationId) {
		this.fromStationId = fStationId;
	}

	public String getFromStation() {
		return fStationName;
	}

	public void setfStationName(String fStationName) {
		this.fStationName = fStationName;
	}

	public String gettStationId() {
		return tStationId;
	}

	public void settStationId(String tStationId) {
		this.tStationId = tStationId;
	}

	public String getToStation() {
		return tStationName;
	}

	public void settStationName(String tStationName) {
		this.tStationName = tStationName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getbYear() {
		return bYear;
	}

	public void setbYear(String bYear) {
		this.bYear = bYear;
	}
	
}
