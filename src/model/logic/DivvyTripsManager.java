package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DobleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	private DobleLinkedList<VOTrip> trips;
	
	public DivvyTripsManager(){
		trips = new DobleLinkedList<VOTrip>();
	}

	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		
	}

	
	public void loadTrips (String tripsFile) throws Exception {
		// TODO Auto-generated method stub
		
		FileReader fr;
		try {
			fr = new FileReader(tripsFile);
			BufferedReader br = new BufferedReader(fr);
			String linea = br.readLine();
			int i = 0;
			
			while(linea!=null ){
				String[] lineaArreglo = linea.split(",");
				if(!lineaArreglo[0].toLowerCase().startsWith("id")){
					String id = lineaArreglo[0];
					String start_time = lineaArreglo[1];
					String end_time = lineaArreglo[2];
					String bikeId = lineaArreglo[3];
					String duration = lineaArreglo[4];
					String fromStationId = lineaArreglo[5];
					String fStationName = lineaArreglo[6];
					String tStationId = lineaArreglo[7];
					String tStationName = lineaArreglo[8];
					String type = lineaArreglo[9];
					String gender = lineaArreglo.length>=11? lineaArreglo[10]: "";
					String bYear = "";
					if(lineaArreglo.length>=12){
					bYear = lineaArreglo[11];
					}

					
					VOTrip viaje = new VOTrip( id,  start_time,  end_time,  bikeId,
							 duration,  fromStationId,  fStationName,
							 tStationId,  tStationName,  type, gender,
							 bYear);
					trips.addAtFirst(viaje);
					System.out.println(i);
					i++;
					
					
				}
				
				linea = br.readLine();
			}
			System.out.println("listo");
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) throws Exception {
		
		DobleLinkedList<VOTrip> porGenero = new DobleLinkedList<VOTrip>();
		Iterator iterador = trips.iterator();
		VOTrip actual = (VOTrip) iterador.next();
		
		int n = 0;
		
		while(iterador.hasNext()){
			
			if(actual.getGender().equalsIgnoreCase(gender)){
				porGenero.addAtFirst(actual);
				n++;
				System.out.println(n);
			}
			actual = (VOTrip) iterador.next();
		}
		
		
		// TODO Auto-generated method stub
		return porGenero;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) throws Exception {
		// TODO Auto-generated method stub
		
		DobleLinkedList<VOTrip> porEstacion = new DobleLinkedList<VOTrip>();
		Iterator iterador = trips.iterator();
		VOTrip actual = (VOTrip) iterador.next();
		
		int n = 0;
		
		while(iterador.hasNext()){

			if(Integer.parseInt(actual.gettStationId())==stationID){
				porEstacion.addAtFirst(actual);
				n++;
				System.out.println(n);
			}
			actual = (VOTrip) iterador.next();
		}
		
		
		// TODO Auto-generated method stub
		return porEstacion;
	}	


}
