package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	public void addAtEnd(T elemento) throws Exception;
	
	public T getElement(int pos);
	
	public T getCurrentELement();
	
	public void delete(T elemento) throws Exception;
	
	public void deleteAtK(int pos);
	
	public T next();
	
	public T previous();

	void addAtFirst(T elemento);
	
	

}
